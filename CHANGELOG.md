# Change Log
All significant changes to this project will be documented in this file.

## [1.0.0](https://github.com/tonystone/tracelog-adaptive-writer/tree/1.0.0)

Initial Public Release.
